/* RENTA LIQUIDA */
select os.co_os as os,f.co_fatura as factura,f.data_emissao,u.co_usuario,f.co_os,f.valor,f.total_imp_inc,sum(round((f.valor - ((f.total_imp_inc/100) * f.valor)))) as liquido,s.brut_salario as bruto,round((f.valor - (f.valor*f.total_imp_inc)) * f.comissao_cn) as comision from cao_os os inner join cao_usuario u on os.co_usuario = u.co_usuario left join cao_fatura f on f.co_os=os.co_os inner join cao_salario s on s.co_usuario = u.co_usuario where u.co_usuario in ('carlos.arruda','renato.pereira') and f.data_emissao between '2007-01-02' and '2018-01-18' group by MONTH(f.data_emissao),YEAR(f.data_emissao) order by f.data_emissao,u.co_usuario asc ;

select os.co_os as os,f.co_fatura as factura,f.data_emissao,u.co_usuario,f.co_os,f.valor,f.total_imp_inc,(round((f.valor - ((f.total_imp_inc/100) * f.valor)))) as liquido,s.brut_salario as bruto,round((f.valor - (f.valor*f.total_imp_inc)) * f.comissao_cn) as comision,round(((f.valor - ((f.total_imp_inc/100) * f.valor)))-((s.brut_salario)+((f.valor - (f.valor*f.total_imp_inc))))) as ganancias,sum(round(((f.valor - ((f.total_imp_inc/100) * f.valor)))-((s.brut_salario)+((f.valor - (f.valor*f.total_imp_inc)))))) as ganancias_totales from cao_os os inner join cao_usuario u on os.co_usuario = u.co_usuario inner join cao_fatura f on f.co_os=os.co_os inner join cao_salario s on s.co_usuario = u.co_usuario where u.co_usuario in ('carlos.arruda','renato.pereira','anapaula.chiodaro','aline.chastel') and f.data_emissao between '2007-01-02' and '2007-06-18' group by MONTH(f.data_emissao),YEAR(f.data_emissao),u.co_usuario order by u.co_usuario,f.data_emissao asc ;

select os.co_os as os,
f.co_fatura as factura,
f.data_emissao,
u.co_usuario,
f.co_os,
f.valor,
f.total_imp_inc,
(round((f.valor - ((f.total_imp_inc/100) * f.valor)))) as liquido,
s.brut_salario as bruto,
round((f.valor - (f.valor*f.total_imp_inc)) * f.comissao_cn) as comision,
round(((f.valor - ((f.total_imp_inc/100) * f.valor)))-((s.brut_salario)+((f.valor - (f.valor*f.total_imp_inc))))) as ganancias ,
sum(select(ganancias)) as total_ganancias
from cao_os os 
inner join cao_usuario u 
on os.co_usuario = u.co_usuario 
inner join cao_fatura f 
on f.co_os=os.co_os 
inner join cao_salario s 
on s.co_usuario = u.co_usuario 
where u.co_usuario 
in ('carlos.arruda','renato.pereira','anapaula.chiodaro','aline.chastel') 
and f.data_emissao 
between '2007-01-02' and '2007-06-18' 
group by MONTH(f.data_emissao),YEAR(f.data_emissao),u.co_usuario 
order by u.co_usuario,f.data_emissao asc ;


select r.* , sum(r.ganancias) as total_ganancias ,sum(r.liquido) as total_liquido ,sum(r.bruto) as total_bruto,sum(r.comision) as total_comision from (


	select r.* from  (
		(select os.co_os as os,f.co_fatura as factura,
			f.data_emissao,
			u.co_usuario,
			f.co_os,
			f.valor,
			f.total_imp_inc,
			(round((f.valor - ((f.total_imp_inc/100) * f.valor)))) as liquido,
			s.brut_salario as bruto,
			round((f.valor - (f.valor*f.total_imp_inc)) * f.comissao_cn) as comision,
			round(((f.valor - ((f.total_imp_inc/100) * f.valor)))-((s.brut_salario)+((f.valor - (f.valor*f.total_imp_inc))))) as ganancias 
			from cao_os os 
			inner join cao_usuario u 
			on os.co_usuario = u.co_usuario 
			inner join cao_fatura f 
			on f.co_os=os.co_os 
			inner join cao_salario s 
			on s.co_usuario = u.co_usuario 
			where u.co_usuario 
			in ('carlos.arruda','renato.pereira','anapaula.chiodaro','aline.chastel') 
			and f.data_emissao 
			between '2007-01-02' and '2007-06-18' 
			group by MONTH(f.data_emissao),YEAR(f.data_emissao),u.co_usuario 
			order by u.co_usuario,f.data_emissao asc 
			) as r
		) ;








	select 
	os.co_os as os,
	f.co_fatura as factura,
	f.data_emissao,
	u.co_usuario,
	f.co_os,
	f.valor,
	f.total_imp_inc,
	(round((f.valor - ((f.total_imp_inc/100) * f.valor)))) as liquido,
	s.brut_salario as bruto,
	round((f.valor - (f.valor*f.total_imp_inc)) * f.comissao_cn) as comision,
	round(((f.valor - ((f.total_imp_inc/100) * f.valor)))-((s.brut_salario)+((f.valor - (f.valor*f.total_imp_inc))))) as ganancias,
	from cao_os os 
	inner join cao_usuario u 
	on os.co_usuario = u.co_usuario 
	inner join cao_fatura f 
	on f.co_os=os.co_os 
	inner join cao_salario s 
	on s.co_usuario = u.co_usuario 
	where u.co_usuario 
	in ('carlos.arruda','renato.pereira','anapaula.chiodaro','aline.chastel') 
	and f.data_emissao 
	between '2007-01-02' and '2007-06-18' 
	group by MONTH(f.data_emissao),YEAR(f.data_emissao),u.co_usuario 
	order by u.co_usuario,f.data_emissao asc 