module.exports = function(app) {

	app.get('/clientes', function (req, res) {
		
		msj = {
			"status":"error",
			"error":"0",
			"msj":"0",
			"type":"error",
			"data":""
		};
		/*COMPRUEBA CONEXIÓN*/
		app.mysql.getConnection(function(err,con) {

			if (err) {
				console.error('error connecting: ' + err.stack);
				msj.msj=err.stack;
				res.status(200)
				.json(msj);
				return;
			}
			/* OBTIENE LA DATA DE CLIENTES*/
			con.query('select co_cliente as id,no_razao from cao_cliente', function(err, rows, fields) {
				if (err) throw err
					msj.msj=err;

				msj.status="ok";
				msj.error=0;
				msj.type="success";
				msj.data = rows;

				res.status(200).json(msj);

			});

		});
	});	

	app.get('/consultores', function (req, res) {
		
		msj = {
			"status":"error",
			"error":"0",
			"msj":"0",
			"type":"error",
			"data":""
		};
		/*COMPRUEBA CONEXIÓN*/
		app.mysql.getConnection(function(err,con) {

			if (err) {
				console.error('error connecting: ' + err.stack);
				msj.msj=err.stack;
				res.status(200)
				.json(msj);
				return;
			}

			con.query('select u.co_usuario,u.no_usuario from cao_usuario u inner join permissao_sistema p on u.co_usuario = p.co_usuario where p.co_sistema = 1 AND p.IN_ATIVO = "s" AND p.co_tipo_usuario in (0,1,2)', function(err, rows, fields) {
				if (err) throw err
					msj.msj=err;

				msj.status="ok";
				msj.error=0;
				msj.type="success";
				msj.data = rows;

				res.status(200).json(msj);

			});

		});
	});



	/**
	 * RELATORIOS
	 * obtiene datos de relatorio
	 * @param  {request} req  parametros enviados desde el front
	 * @param  {String}  res  respuesta json	
	 */
	 app.post('/relatorio', function (req, res) {
	 	var fillUsuarios = '';
	 	var relatorio    = [];
	 	var resAux 		 = [];
	 	var obj 		 = {
	 		usuario:'',
	 		data:[]
	 	};
	 	var totalLiquido = 0;
	 	var totalBruto = 0;
	 	var totalLiquido = 0;
	 	var totalComision = 0;
	 	var totalGanancia = 0;
	 	msj = {
	 		"status":"error",
	 		"error":"0",
	 		"msj":"0",
	 		"type":"error",
	 		"data":""
	 	};

	 	/* VALIDACION DE PARAMETROS DE ENTRADA NULOS */
	 	if(req.body.fecha_inicio === '' ){

	 	}

	 	inicio =req.body.fecha_inicio;
	 	termino =req.body.fecha_termino;
	 	usuarios = req.body.usuarios;
	 	usrArray = [];

	 	/* LIMPIA LOS USUARIOS NULOS */
	 	for(u in usuarios){
	 		if(usuarios[u] != null){
	 			usrArray.push(usuarios[u]);
	 		}
	 	}

	 	/* CONSTRUYE FILTRO DE USUARIOS */
	 	if(usrArray.length>1){

	 		for (i = 0 ;i<usrArray.length;i++ ){
	 			if(usrArray[i] != null){
	 				if(i<=usrArray.length-2){
	 					fillUsuarios+="'"+usrArray[i]+"',"
	 				}else{
	 					fillUsuarios+="'"+usrArray[i]+"'"
	 				}
	 			}
	 		}

	 	}else{
	 		fillUsuarios+="'"+usrArray[0]+"'"
	 	}

	 	/*COMPRUEBA CONEXIÓN*/
	 	app.mysql.getConnection(function(err,con) {

	 		if (err) {
	 			console.error('error connecting: ' + err.stack);
	 			msj.msj=err.stack;
	 			res.status(200)
	 			.json(msj);
	 			return;
	 		}
	 		/* OBTIENE DATOS DE DB*/
	 		var query ="select os.co_os as os,f.co_fatura as factura,f.data_emissao,u.co_usuario,(case when u.no_usuario is null then u.co_usuario else u.no_usuario end) as nombre,f.co_os,f.valor,f.total_imp_inc,(round((f.valor - ((f.total_imp_inc/100) * f.valor)))) as liquido,s.brut_salario as bruto,round((f.valor - (f.valor*f.total_imp_inc)) * f.comissao_cn) as comision,round(((f.valor - ((f.total_imp_inc/100) * f.valor)))-((s.brut_salario)+((f.valor - (f.valor*f.total_imp_inc))))) as ganancias from cao_os os inner join cao_usuario u on os.co_usuario = u.co_usuario inner join cao_fatura f on f.co_os=os.co_os inner join cao_salario s on s.co_usuario = u.co_usuario where u.co_usuario in ("+fillUsuarios+") and f.data_emissao between '"+inicio+"' and '"+termino+"' group by MONTH(f.data_emissao),YEAR(f.data_emissao),u.co_usuario order by u.co_usuario,f.data_emissao asc";
	 		con.query(query, function(err, rows, fields) {
	 			if (err) throw err
	 				msj.msj=err;

	 			msj.status="ok";
	 			msj.error=0;
	 			msj.type="success";

	 			console.log(query);

	 			if(rows.length>0){
	 				for(u in usrArray){

	 					for(r in rows){
	 						totalBruto=0;
	 						totalLiquido=0;
	 						totalComision=0;
	 						totalGanancia=0;
	 						if(rows[r].co_usuario === usrArray[u]){
	 							resAux.push(rows[r]);
	 							for(ritem in rows[r]){
	 								console.log(rows[r])
	 								totalLiquido 	+= rows[r].liquido;
	 								totalBruto 		+= rows[r].bruto;
	 								totalComision 	+= rows[r].comision;
	 								totalGanancia 	+= rows[r].ganancias;
	 							}

	 							obj.usuario = (rows[r].nombre==null || rows[r].nombre=='')?rows[r].co_usuario:rows[r].nombre;
	 							obj.total_liquido 	= totalLiquido;
	 							obj.total_bruto 	= totalBruto;
	 							obj.total_comision 	= totalComision;
	 							obj.total_ganancia 	= totalGanancia;

	 						}
	 					}

	 					if(resAux.length >0){
	 						obj.data 	= resAux;
	 						relatorio.push(obj);
	 					}else{
	 						/* SI NO SE ENCONTRARON DATOS EN LA DB, SE RETORNA UNA RESPUESTA DUMMY */
	 						obj.usuario = usrArray[u];
	 						obj.total_liquido =0;
	 						obj.total_bruto =0;
	 						obj.total_comision =0;
	 						obj.total_ganancia =0;
	 						obj.data 	= [
	 						{
	 							"bruto":0,
	 							"co_os":0,
	 							"co_usuario":0,
	 							"comision":0,
	 							"data_emissao":0,
	 							"factura":0,
	 							"ganancias":0,
	 							"liquido":0,
	 							"nombre":0,
	 							"os":0,
	 							"total_imp_inc":0,
	 							"valor":0
	 						}
	 						];
	 						relatorio.push(obj);
	 					}

	 					resAux			= [];
	 					obj				= {};
	 				}
	 			}else{
	 				for(u in usrArray){
	 					/* SI NO SE ENCONTRARON DATOS EN LA DB, SE RETORNA UNA RESPUESTA DUMMY */
	 					console.log(usrArray[u])
	 					obj.usuario 		= 	usrArray[u];
	 					obj.total_liquido 	=	0;
	 					obj.total_bruto 	=	0;
	 					obj.total_comision 	=	0;
	 					obj.total_ganancia 	=	0;
	 					obj.data 	= [
	 					{
	 						"bruto":0,
	 						"co_os":0,
	 						"co_usuario":0,
	 						"comision":0,
	 						"data_emissao":0,
	 						"factura":0,
	 						"ganancias":0,
	 						"liquido":0,
	 						"nombre":0,
	 						"os":0,
	 						"total_imp_inc":0,
	 						"valor":0
	 					}
	 					];
	 					relatorio.push(obj);
	 				}
	 			}

	 			msj.data = relatorio;
	 			relatorio=[];
	 			res.status(200).json(msj);

	 		});

	 	});
	 });




/**
	 * GRAFICOS
	 * obtiene datos para poblar grafico de barras
	 * @param  {request} req  parametros enviados desde el front
	 * @param  {String}  res  respuesta json	
	 */
	 app.post('/grafico', function (req, res) {
	 	var fillUsuarios = '';
	 	var grafico    = [];
	 	var resAux 		 = [];
	 	var obj 		 = {
	 		label:[],
	 		fechas:[],
	 		data:[],
	 		sindata:false
	 	};

	 	msj = {
	 		"status":"error",
	 		"error":"0",
	 		"msj":"0",
	 		"type":"error",
	 		"data":""
	 	};
	 	/* OBTIENE DATA DEL REQUEST */
	 	inicio 		=	req.body.fecha_inicio;
	 	termino 	=	req.body.fecha_termino;
	 	usuarios 	= 	req.body.usuarios;
	 	usrArray 	= 	[];

	 	/* LIMPIA LOS USUARIOS NULOS */
	 	for(u in usuarios){
	 		if(usuarios[u] != null){
	 			usrArray.push(usuarios[u]);
	 		}
	 	}

	/* CONSTRUYE FILTRO DE USUARIOS */
	if(usrArray.length>1){

		for (i = 0 ;i<usrArray.length;i++ ){
			if(usrArray[i] != null){
				if(i<=usrArray.length-2){
					fillUsuarios+="'"+usrArray[i]+"',"
				}else{
					fillUsuarios+="'"+usrArray[i]+"'"
				}
			}
		}
	}else{
		fillUsuarios+="'"+usrArray[0]+"'"
	}

	/*COMPRUEBA CONEXIÓN*/
	app.mysql.getConnection(function(err,con) {

		if (err) {
			console.error('error connecting: ' + err.stack);
			msj.msj=err.stack;
			res.status(200)
			.json(msj);
			return;
		}
		var fechas = [];
		var gananciasbyUsuarios = [];
		var arrayGanancias 		= [];
		var totalBruto          = 0;
		
		/* OBTENGO LAS FECHAS */
		var query ="select concat(YEAR(f.data_emissao),'-',MONTH(f.data_emissao)) as fechas from cao_os os inner join cao_usuario u on os.co_usuario = u.co_usuario inner join cao_fatura f on f.co_os=os.co_os inner join cao_salario s on s.co_usuario = u.co_usuario where u.co_usuario in ("+fillUsuarios+") and f.data_emissao between '"+inicio+"' and '"+termino+"'  group by MONTH(f.data_emissao),YEAR(f.data_emissao) order by f.data_emissao asc"
		con.query(query, function(err, rows, fields) {
			if (err) throw err
				console.log(err);				
			// console.log("QUERY FECHAS",query);
			
			if(rows.length>0){
				for(r in rows){
					fechas.push(rows[r].fechas);
				}
			}
			
			/* SI EXISTEN FECHAS OBTENGO LA GANANCIA POR USUARIO*/
			if(fechas.length >0){
				/* OBTENGO LA GANANCIA POR USUARIO */
				var query ="select u.co_usuario,u.no_usuario,s.brut_salario as bruto, concat(YEAR(f.data_emissao),'-',MONTH(f.data_emissao)) as data_emissao ,(case when u.no_usuario is null then u.co_usuario else u.no_usuario end) as nombre,round(((f.valor - ((f.total_imp_inc/100) * f.valor)))-((s.brut_salario)+((f.valor - (f.valor*f.total_imp_inc))))) as ganancias from cao_os os inner join cao_usuario u on os.co_usuario = u.co_usuario inner join cao_fatura f on f.co_os=os.co_os inner join cao_salario s on s.co_usuario = u.co_usuario where u.co_usuario in ("+fillUsuarios+") and f.data_emissao between '"+inicio+"' and '"+termino+"' group by MONTH(f.data_emissao),YEAR(f.data_emissao),u.co_usuario order by u.co_usuario,f.data_emissao asc";
				con.query(query, function(err, rows, fields) {
					if (err) throw err
						msj.msj=err;
					
					if(rows.length>0){

						/* RECORRO LOS USUARIOS RECIBIDOS POR PARAMETROS */
						for(u in usrArray){
							for(f in fechas){
								
								/*RECORRO LA DATA OBTENIDA DE LA BD */
								for(r in rows){

									/* CUANDO EL USUARIO RECIBIDO POR PARAMETRO ES IGUAL AL ROW ITEM DE LA DB */
									if(rows[r].co_usuario === usrArray[u]){
										obj.label[u] 	= (rows[r].nombre==null || rows[r].nombre=='')?rows[r].co_usuario:rows[r].nombre;
										obj.fechas 		= fechas;

										if( fechas[f] === rows[r].data_emissao){
											gananciasbyUsuarios[f] = rows[r].ganancias
											totalBruto+=rows[r].bruto;
										}
									}

								};
								/* SI NO HAY COINCIDENCIA DE NOMBRES SE ASUME EL NOMBRE COMO EL ID DEL USUARIO */
								if(typeof obj.label[u] === 'undefined'){
									obj.label[u]=usrArray[u];
								}
							}

							if(gananciasbyUsuarios.length===0){
								for(f in fechas){
									gananciasbyUsuarios[f]=0;
								}
								arrayGanancias.push(gananciasbyUsuarios);
							}else{
								var a = [];
								/* LIMPIA LOS VALORES NULOS Y LOS REEMPLAZA POR UN 0 */
								for(var i = 0;i <gananciasbyUsuarios.length;i++ ){
									if(gananciasbyUsuarios[i] != null && typeof gananciasbyUsuarios[i] !== 'undefined'){
										a[i]=gananciasbyUsuarios[i];
									}else{
										a[i]=0;
									}
								}
								arrayGanancias.push(a);
							}

							gananciasbyUsuarios=[];
						}

						/* SI TENGO DATA PARA LOS USUARIOS */
						if(arrayGanancias.length >0){
							obj.data  = arrayGanancias;
							obj.bruto = Math.round(parseInt(totalBruto)/usrArray.length);
							grafico.push(obj);
						}else{
							for(u in usrArray){
								console.log(usrArray[u])
								obj.label[u] 		= 	usrArray[u];
								obj.fechas 			= 	fechas;

								for(f in fechas){
									gananciasbyUsuarios[f]=0;
								}

								arrayGanancias.push(gananciasbyUsuarios);
								obj.data = arrayGanancias;
								obj.bruto = 0;
								arrayGanancias=[];
							}

							grafico.push(obj);
						}
						
					}else{
						/* SI NO HAY DATOS, DEVUELVO UN ARRAY FORMATEADO VACIO*/
						console.log("no hay data para",usrArray[u]);
						for(u in usrArray){
							console.log(usrArray[u])
							obj.label[u] 			= 	usrArray[u];
							obj.fechas 				= 	fechas;
							obj.bruto 				= parseInt(totalBruto)/usrArray.length;
							
							for(f in fechas){
								gananciasbyUsuarios[f]=0;
							}

							arrayGanancias.push(gananciasbyUsuarios);
							obj.data = arrayGanancias;
							arrayGanancias=[];
						}
						grafico.push(obj);
					}

					msj.data = grafico;
					res.status(200).json(msj);

				});
			}else{
				/* SI NO HAY FECHAS */
				console.log("no hay FECHAS para",usrArray[u]);
				for(u in usrArray){
					console.log(usrArray[u])
					obj.label[u] 		= usrArray[u];
					obj.fechas 			= [];
					obj.data 			= [];
					obj.bruto 			= 0;
					obj.sindata 		= true;
				}
				
				grafico.push(obj);
				console.log("grafico",grafico);
				msj.data = grafico;
				res.status(200).json(msj);
			}
		});
});
});

/**
	 * PIZZA
	 * obtiene datos para poblar grafico de pizza
	 * @param  {request} req  parametros enviados desde el front
	 * @param  {String}  res  respuesta json	
	 */

	 app.post('/pizza', function (req, res) {
	 	var fillUsuarios 	= '';
	 	var grafico    		= [];
	 	totalBruto 			= 0
	 	obj 		 	= {
	 		labels:[],
	 		bruto:[],
	 		sindata:false
	 	};
	 	var label = [];
	 	var bruto = [];
	 	var l     = [];
	 	var b     = [];
	 	msj = {
	 		"status":"error",
	 		"error":"0",
	 		"msj":"0",
	 		"type":"error",
	 		"data":""
	 	};
	 	/* OBTIENE DATA DEL REQUEST */
	 	inicio 		=	req.body.fecha_inicio;
	 	termino 	=	req.body.fecha_termino;
	 	usuarios 	= 	req.body.usuarios;
	 	usrArray 	= 	[];

	 	/* LIMPIA LOS USUARIOS NULOS */
	 	for(u in usuarios){
	 		if(usuarios[u] != null){
	 			usrArray.push(usuarios[u]);
	 		}
	 	}
	// console.log("USUARIOS",usuarios);

	/* CONSTRUYE FILTRO DE USUARIOS */
	if(usrArray.length>1){

		for (i = 0 ;i<usrArray.length;i++ ){
			if(usrArray[i] != null){
				if(i<=usrArray.length-2){
					fillUsuarios+="'"+usrArray[i]+"',"
				}else{
					fillUsuarios+="'"+usrArray[i]+"'"
				}
			}
		}
	}else{
		fillUsuarios+="'"+usrArray[0]+"'"
	}
	/*COMPRUEBA CONEXIÓN*/
	app.mysql.getConnection(function(err,con) {
		if (err) {
			console.error('error connecting: ' + err.stack);
			msj.msj=err.stack;
			res.status(200)
			.json(msj);
			return;
		}

		var query ="select u.no_usuario,u.co_usuario,sum((round((f.valor - ((f.total_imp_inc/100) * f.valor))))) as liquida from cao_os os inner join cao_usuario u on os.co_usuario = u.co_usuario inner join cao_fatura f on f.co_os=os.co_os inner join cao_salario s on s.co_usuario = u.co_usuario where u.co_usuario in ("+fillUsuarios+")and f.data_emissao between '"+inicio+"' and '"+termino+"' group by u.co_usuario order by u.co_usuario";
		

		con.query(query, function(err, rows, fields) {
			if (err) throw err
				msj.msj=err;

			if(rows.length>0){
				console.log(rows);

				for(r in rows){
					totalBruto+=rows[r].liquida;
				}

				for(u in usrArray){
					/*RECORRO LA DATA OBTENIDA DE LA BD */
					for(r in rows){
						/* CUANDO EL USUARIO RECIBIDO POR PARAMETRO ES IGUAL AL ROW ITEM DE LA DB  */
						if(rows[r].co_usuario === usrArray[u]){

							label[u] 			= 	usrArray[u];
							bruto[u] 			= 	Math.round((parseInt(rows[r].liquida)/totalBruto)*100);

						}
					}
					/* SI NO HAY COINCIDENCIA DE NOMBRES SE ASUME EL NOMBRE COMO EL ID DEL USUARIO */
					if(typeof label[u] === 'undefined'){
						label[u]=usrArray[u];
					}
				}
				/* LIMPIA LOS VALORES NULOS Y LOS REEMPLAZA POR UN 0 */
				for(var i = 0;i <label.length;i++ ){
					if(bruto[i] != null && typeof bruto[i] !== 'undefined'){
						b[i]=bruto[i];
					}else{
						b[i]=0;
					}
				}


				obj.labels				= 	label;
				obj.bruto 				= 	b;
				grafico.push(obj);
				obj={};
				label=[];
				bruto=[];
			}else{
				/* SI NO HAY DATOS, DEVUELVO UN ARRAY FORMATEADO VACIO*/
				console.log("no hay data para",usrArray[u]);
				for(u in usrArray){
					console.log(usrArray[u])
					obj.labels[u]				= 	usrArray[u];
					obj.bruto[u] 				= 	0
					obj.sindata					= 	true;
				}
				grafico.push(obj);
			}

			console.log("pizzs",grafico);
			msj.data = grafico;
			res.status(200).json(msj);

		});
	});


});



	}