// var portSocket   = 3111;

// var express         = require('express');
var port        = 3113;
// var hostExpress     = 'http://desa10k.acepta.com';

var express     = require('express');  
var app         = express();  
var server      = require('http').createServer(app);  
var mysql       = require('mysql');


var pool  = mysql.createPool({
  connectionLimit : 100,
  host     : 'localhost',
  user     : 'root',
  password : 'root',
  database : 'agence',
  port     : '8889'
  // port     : '3306'
});

app.mysql       = pool;

// var io = require('socket.io')(server);

/* SE LEVANTA EL SERVIDOR HTTP EN EL PUERTO Y HOST CONFIGURADO MAS ARRIBA */
// var http = require('http').Server(app);
/* SE ASOCIA EL LISTENER DE SOCKET AL SERVIDOR RECIEN CREADO. */
// var io = require('socket.io')(http);




//https.createServer(sslOptions, server).listen(8443)

/* configuracion de accesos */
cors = require('cors');

/* instancia de servidores*/

app.use(cors());

/* librerias para manejo de JSON */
var bodyParser  = require('body-parser');
var jsonParser  = bodyParser.json();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


// atachando socket.io al app
// app.io = io;

// incluyendo las distintas apps en routes
require('./apps')(app);


server.listen(port, function() {
  console.log(' Agence Up On Port:'+port);
});
// app.listen(port, function () {
  // console.log('Node Express Up On Port:'+port);
// });

//server.listen(portSocket);
